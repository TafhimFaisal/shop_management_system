<?php $title = __('title.product')?>
@extends('layouts.master')
@section('content')
@if(isset($product)) 
    <?php $action="/product/$product->id";$method ="put";$name = "$product->name";$buttonName = "Update Product"?> 
    @else 
    <?php $action="/product";$method = "post";$name = " ";$buttonName = "Add Product" ?> 
@endif
@include('layouts.includes.massage')
<div class="card">
    <div class="card-header">
        <form action={{$action}} method="post">
            @csrf
            @method($method)
            @if(isset($product))
                <input type="hidden" name="id" value="{{$product->id}}">
            @endif 
            <div class="row">
                <div class="col-md-10 col-sm-6">
                    <div class="form-group">
                        <input type="text" name="name" value="{{ old('name') ? old('name') : $name }}" class="form-control @error('name') is-invalid @enderror" id="product" placeholder="Enter product name..">
                        @include('layouts.includes.errors',['name'=>'name'])
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <button type="submit" style="width:100%" class="btn btn-info">{{$buttonName}}</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table style="width:100%" id="products_table" class="table table-striped table-bordered dt-responsive nowrap">
        <thead>
        <tr>
            <th>ID</th>
            <th width="70%">Name</th>
            <th>Quantity</th>
            <th width="15%">Action</th>
        </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Action</th>
        </tr>
        </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.col -->
</div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
@endsection
@push('scripts')
    <script>
       $(function () {
            $("#products_table").DataTable({
                "ordering": true,
                "processing": true,
                "serverSide": true,
                "ajax":{
                        "url": "{{ url('data/products') }}",
                        "dataType": "json",
                        "type": "POST",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
                "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "quantity" },
                    { "data": "action" },
                ]
            });
        });
    </script>
@endpush
