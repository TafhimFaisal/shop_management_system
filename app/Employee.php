<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Due;
use App\Order;
use DB;

class Employee extends Model
{
    protected $fillable = [
        'name','email','phone'
    ];

    public function dueHistory(){
        return $this->hasMany('App\DueHistory','employee_id','id');
    }
    public function due(){
        return $this->hasOne(Due::class,'employee_id','id');
    }
    public function order(){
        return $this->hasMany(Order::class);
    }
    public function createDueHistory(){
        return $this->due()->create(['due' => 0]);
    }
}
