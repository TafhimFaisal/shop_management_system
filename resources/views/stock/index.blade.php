<?php $title = __('title.stock')?>
@extends('layouts.master')
@section('content')
  <div class="card card-default">
      <form action="\stock" method="post">
        @csrf
        @method('post')
        <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="product" class="col-form-label">Quantity :</label>
                      <select id='product' name="product_id" class="form-control @error('product_id') is-invalid @enderror">
                          <option value="">- Search Product -</option>
                      </select>
                      @include('layouts.includes.errors',['name'=>'product_id'])
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="quantity" class="col-form-label">Quantity :</label>
                    <input type="number" name="quantity" class="form-control @error('quantity') is-invalid @enderror" id="quantity" placeholder="Enter quantity..">
                    @include('layouts.includes.errors',['name'=>'quantity'])
                  </div>
              </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Add Stock</button>
        </div>
      </form>
  </div>
  
@endsection
@push('scripts')
  <script>
      
    $(document).ready(function(){

      $("#product").select2({
        ajax: { 
          url: '{{ url("search/products/option")}}',
          // type: "post",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              searchTerm: params.term // search term
            };
          },
          processResults: function (response) {
            return {
              results: response
            };
          },
          cache: true
        },
        theme: 'bootstrap4'
      });

    });

  </script>
@endpush