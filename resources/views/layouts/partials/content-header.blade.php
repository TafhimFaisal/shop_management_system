<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$title}}</h1>
            </div>
            <div class="col-sm-6">
                <?php $url = explode(" ",str_replace('/',' ',$_SERVER['REQUEST_URI'])) ?>
                
                <ol class="breadcrumb float-sm-right">
                    @foreach ($url as $breadcrumb)
                        <li class="breadcrumb-item"><a href="#">{{$breadcrumb}}</a></li>
                    @endforeach
                    <li class="breadcrumb-item active">{{strtolower(str_replace(' ','-',$title))}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
