<?php


Route::get('/', function () {
    return view('auth.login');
});
Route::get('home','DashboardController@index');
Route::get('due','DashboardController@due');
Route::post('home/datatable','DashboardController@datatable');
Route::post('due/datatable','DashboardController@duedatatable');

//--product--//
Route::resource('product','ProductController');

Route::post('data/products','ProductController@datatable');
Route::get('search/products/option','ProductController@option');

Route::post('search/products','ProductController@search');

Route::get('data/product/history/sells/{id}','ProductController@sells');
Route::get('data/product/history/stocks/{id}','ProductController@stocks');

Route::get('datatable/product/history/sells/{id}','ProductController@datatableSells');
Route::get('datatable/product/history/stocks/{id}','ProductController@datatableStocks');
//--product--//

//--stock--//
Route::get('stock','StockController@index');
Route::post('stock','StockController@store');
//--stock--//


//---employee---//
Route::resource('employee','EmployeeController');
Route::post('data/employees','EmployeeController@datatable');

Route::get('data/employee/history/dues/{id}','EmployeeController@due');
Route::get('data/employee/history/orders/{id}','EmployeeController@order');
Route::get('data/employee/history/paid-dues/{id}','EmployeeController@paiddue');

Route::get('datatable/employee/history/dues/{id}','EmployeeController@datatableDue');
Route::get('datatable/employee/history/orders/{id}','EmployeeController@datatableOrder');
Route::get('datatable/employee/history/datatable/paid-dues/{id}','EmployeeController@datatablePaidDue');

Route::post('import/employee','EmployeeController@import');
Route::get('search/employee/option','EmployeeController@option');
//---employee---//

//---users---//
Route::resource('user','UserController');

Route::post('data/users','UserController@datatable');
Route::post('change/user/password','UserController@changePassword');

Route::get('data/users/history/stocks/{id}','UserController@stock');
Route::get('data/users/history/orders/{id}','UserController@order');
Route::get('data/users/history/collections/{id}','UserController@collection');

Route::get('datatable/users/history/stocks/{id}','UserController@datatableStock');
Route::get('datatable/users/history/orders/{id}','UserController@datatableOrder');
Route::get('datatable/users/history/collections/{id}','UserController@datatableCollection');
//---users---//

//--order--//
Route::get('order','OrderController@index');
Route::post('order','OrderController@store');
//--order--//


//--collection--//
Route::get('collection','CollectionController@index');
Route::post('collection','CollectionController@store');
Route::get('search/due','CollectionController@dueSearch');
//--collection--//

Auth::routes();
