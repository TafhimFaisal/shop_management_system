<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DueHistory extends Model
{
    protected $fillable = [
        'due',
        'order_id',
        'employee_id'
    ];

    public function employee(){
        $this->belongsTo('App\Employee','employee_id');
    }
    public function order(){
        $this->belongsTo('App\Order');
    }
}
