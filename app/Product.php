<?php

namespace App;
use App\Stock;
use App\StockHistory;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name'];
    public function stock(){
        return $this->hasOne(Stock::class);
    }
    public function stockHistory(){
        return $this->hasMany(StockHistory::class); 
    }
    public function addstockHistory($quantity = 0){ 
        $this->stockHistory()->create([
            'user_id' => auth()->user()->id,
            'quantity' => $quantity
        ]);
    }
    public function addStock(){ 
        $this->addstockHistory();
        $this->stock()->create(['quantity' => 0]); 
    }

    public function order(){ 
        $this->hasMany('App\Order'); 
    }

}
