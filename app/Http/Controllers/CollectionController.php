<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Due;
use Illuminate\Http\Request;
use App\Http\Requests\CollectionRequest;
use DB;

class CollectionController extends Controller
{
   
    public function index()
    {
        return view('collection.index');
    }
    
    public function dueSearch(){
        $id = request()->all()['query'];
        return DB::table('dues')->where('employee_id',$id)->get()->first()->due;
    }

    public function store(CollectionRequest $request)
    {
        $previousDue = Due::where('employee_id',$request->employee_id)->first()->due;
        $paing = $request->paid;

        if($previousDue>=$paing){
            Collection::create($request->all())->reductDue($request);
            return redirect('collection')->with('success','Successfull');
        }else{
            return redirect('collection')->with('info','Previous Due is less then what is being paid');
            
        }
    }

}
