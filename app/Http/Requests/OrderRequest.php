<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id'=>"required",
            'user_id'=>"required",
            'product_id'=>"required",
            'unit'=>"required|min:0|numeric",
            'per_unit_price'=>"required|min:0|numeric",
            'paid'=>"required|min:0|numeric"
        ];
        
    }
    public function messages()
    {
        return [
            'employee_id.required' => 'Employee is required',
            'product_id.required' => 'Product is required',
            
            'unit.required' => 'Quantity is required',
            'unit.min' => 'Quantity Cannot be less then 0',
            'unit.numeric' => 'Quantity must be numeric',
            
            'per_unit_price.required' => 'Per-Unit Price is required',
            'per_unit_price.min' => 'Per-Unit Price Cannot be less then 0',
            'per_unit_price.numeric' => 'Per-Unit Price must be numeric',

            'paid.required' => 'Payment is required',
            'paid.min' => 'Payment Cannot be less then 0',
            'paid.numeric' => 'Payment must be numeric'
        ];
    }
}
