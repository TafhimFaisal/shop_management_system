<?php $title = __('title.user')?>
@extends('layouts.master')
@section('content')
@include('layouts.includes.massage')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-10">
                    <h3 class="card-title"></h3>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a role="button" href="/user/create" class="btn btn-success btn-sm" style="width:100%">Add User</a>
                </div>
            </div> 
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table style="width:100%" id="user_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th width="15%">Action</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th width="15%">Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
   
@endsection

@push('scripts')
    <script>
       $(function () {
        $("#user_table").DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('data/users') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "action" }
              ]
        });
      });
    </script>
@endpush