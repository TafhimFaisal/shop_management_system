<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id'=>'required',
            'user_id'=>'required',
            'paid'=>'required|min:0|numeric'
        ];
    }

    public function messages()
    {
        return [
            'employee_id.required' => 'Employee is required',
            'paid.required' => 'Pament amount is required',
            'paid.min' => 'Pament cannot be more less then 0',
            'paid.numeric' => 'Pament must be in numeric'
        ];
    }
}
