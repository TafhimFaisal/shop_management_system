@include('layouts.partials.__heade')
@include('layouts.partials.header')
@include('layouts.partials.sidebar')
<div class="content-wrapper">

    @include('layouts.partials.content-header')
    @include('layouts.partials.content')

</div>

@include('layouts.partials.footer')
@include('layouts.partials.__foot')