<?php $title = __('title.employee-edit')?>
@extends('layouts.master')
@section('content')
<div class="card card-default">
        <form action="/employee/{{$employee->id}}" method="post">
            @method('put')
            @csrf
            <input type="hidden" name="id" value="{{$employee->id}}">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Name:<small class="text-danger">*</small></label>
                            <div class="">
                                <input type="text" value="{{ old('name') ? old('name') : $employee->name  }}" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter name..">
                                @include('layouts.includes.errors',['name'=>'name'])
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email:<small class="text-danger">*</small></label>
                            <div class="">
                                <input type="text" value="{{ old('email') ? old('email') : $employee->email  }}" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter Email..">
                                @include('layouts.includes.errors',['name'=>'email'])
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="phone" class="col-form-label">Phone :<small class="text-danger">*</small></label>
                            <div class="">
                                <input type="number" value="{{ old('phone') ? old('phone') : $employee->phone  }}" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="Enter Phone Number..">
                                @include('layouts.includes.errors',['name'=>'phone'])
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Update</button>
            </div>
        </form>
    </div>
@endsection