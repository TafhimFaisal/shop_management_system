<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\User;
use App\Due;
use App\Order;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index()
    {
        $totale_employee = Employee::all()->count();
        $totale_user = User::all()->count();
        $totale_due = Due::all()->sum('due');
        $totale_unit = Order::all()->sum('unit');
        $totale_perunit_price = Order::all()->sum('per_unit_price');
        $totale_price = $totale_unit*$totale_perunit_price;
        
        return view('index')
                ->with("totale_employee",$totale_employee)
                ->with("totale_user",$totale_user)
                ->with("totale_due",$totale_due)
                ->with("totale_price",$totale_price);
    }

    public function due()
    {
        $totale_employee = Employee::all()->count();
        $totale_user = User::all()->count();
        $totale_due = Due::all()->sum('due');
        $totale_unit = Order::all()->sum('unit');
        $totale_perunit_price = Order::all()->sum('per_unit_price');
        $totale_price = $totale_unit*$totale_perunit_price;
        
        return view('due')
                ->with("totale_employee",$totale_employee)
                ->with("totale_user",$totale_user)
                ->with("totale_due",$totale_due)
                ->with("totale_price",$totale_price);
    }

    public function datatable()
    {

        return datatables()
                ->of(Order::all())
                ->addColumn('product','{{App\Product::findorFail($product_id)->name}}')
                ->addColumn('quantity','{{App\Order::where("product_id",$product_id)->sum("unit")}}')
                ->addColumn('sells','{{App\Order::where("product_id",$product_id)->sum("unit")*App\Order::where("product_id",$product_id)->sum("per_unit_price")}}')
                ->toJson();
        
    }

    public function duedatatable()
    {
        return datatables()
                ->of(Due::all())
                ->addColumn('employee','{{App\Employee::findorFail($employee_id)->name}}')
                ->toJson();
        
    }
 
}
