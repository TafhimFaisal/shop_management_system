<?php

namespace App\Http\Controllers;

use App\Order;
use App\Due;
use App\Stock;

use App\DueHistory;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('order.index');
    }
    public function store(OrderRequest $request)
    {
        $order = $request->all();
        $product = Stock::findorFail($order['product_id']);
        
        if($product->quantity>$order['unit']){
            $order['due'] = ($order['unit']*$order['per_unit_price']) - $order['paid'];
            Order::create($order)->addDue($order);
            $product->quantity = ($product->quantity-$order['unit']);
            $product->save();
            return redirect('order')->with('success','successfull');
        }else{
            return redirect('order')->with('info','Incaficiant Stock');
        }

    }

}
