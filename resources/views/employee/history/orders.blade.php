<?php $title = __('title.employee-history-order')?>
@extends('layouts\master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1>Current Due:{{$due}}</h1>
        </div>
        <div class="card-body">
            <table style="width:100%" id="employee_order_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Paid</th>
                <th>Due</th>
                <th>Entry Date</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Paid</th>
                <th>Due</th>
                <th>Entry Date</th>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
       $(function () {
        $("#employee_order_table").DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": '{{ url("datatable/employee/history/orders/$id")}}',
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}" }
                   },
            "columns": [
                { "data": "product" },
                { "data": "unit" },
                { "data": "price" },
                { "data": "paid" },
                { "data": "due" },
                { "data": "created_at" }
              ]
        });
      });
    </script>
@endpush