<?php $title = __('title.user-create')?>
@extends('layouts.master')
@section('content')
<div class="card card-default">
    <form action="\user" method="post">
        @csrf
        @method('post')
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Name:</label>
                        <div class="">
                            <input type="text" value="{{ old('name') }}" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter name..">
                            @include('layouts.includes.errors',['name'=>'name'])
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email:</label>
                        <div class="">
                            <input type="text" value="{{old('email') }}" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter Email..">
                            @include('layouts.includes.errors',['name'=>'email'])
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone" class="col-form-label">Phone :</label>
                        <div class="">
                            <input type="text" value="{{ old('phone') }}" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="Enter Phone Number..">
                            @include('layouts.includes.errors',['name'=>'phone'])
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password :</label>
                        <div class="">
                            <input type="password" value="{{ old('password') }}" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Enter password...">
                            @include('layouts.includes.errors',['name'=>'password'])
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Add</button>
        </div>
    </form>
</div>
   
@endsection