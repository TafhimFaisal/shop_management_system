<?php $title = __('title.user-history-stock')?>
@extends('layouts.master')
@section('content')
   
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table style="width:100%" id="user_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Entry Date</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Entry Date</th>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
    
</section>
@endsection
@push('scripts')
    <script>
       $(function () {
        $("#user_table").DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": '{{ url("datatable/users/history/stocks/$id")}}',
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}" }
                   },
            "columns": [
                { "data": "product_name" },
                { "data": "quantity" },
                { "data": "entry_date" }
              ]
        });
      });
    </script>
@endpush