<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->all());
        if($this->id!=0){
            return [
                'name'=>'required',
                'email'=>'required|email|unique:employees,email,'.$this->id,
                'phone'=>'required|min:11|max:11|unique:employees,phone,'.$this->id
            ];
        }else{
            return [
                'name'=>'required',
                'email'=>'required|email|unique:employees,email',
                'phone'=>'required|min:11|max:11|unique:employees,phone'
            ];
        }
    }
}
