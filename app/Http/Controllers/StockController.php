<?php

namespace App\Http\Controllers;

use App\Stock;
use App\StockHistory;
use Illuminate\Http\Request;
use App\Http\Requests\StockRequest;

class StockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('stock.index');
    }

    public function store(StockRequest $request)
    {
        $product = Stock::where('product_id',$request->product_id)->first();
        $product->update(['quantity'=>($request->quantity+$product->quantity)]);
        StockHistory::create([
            'product_id'=>$request->product_id,
            'user_id'=>auth()->user()->id,
            'quantity'=>$request->quantity,
        ]);
        return redirect('stock');
    }

    
}
