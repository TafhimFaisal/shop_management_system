<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DueHistory;
use App\Due;
use App\Product;
use DB;
class Order extends Model
{
    protected $fillable = [
        'employee_id',
        'user_id',
        'product_id',
        'unit',
        'per_unit_price',
        'paid',
        'due'
    ];

    public function dueHistory(){
        $this->hasMany('App\DueHistory');
    }
    public function due(){
        $this->hasOne('App\Due');
    }

    public function user(){
        $this->belongsTo('App\User');
    }
    public function product(){
        $this->belongsTo('App\Product');
    }
    public function employee(){
        $this->belongsTo('App\Employee');
    }

    public function addOrderHistory($order){
        DueHistory::create([
            'order_id' => $this->latest()->first()->id,
            'due' => $order['due'],
            'employee_id' => $order['employee_id'],
        ]);
    }
    
    public function addDue($order){
        if($order['due']!=0){
            $this->addOrderHistory($order);
            $due = DB::table('dues')->where('employee_id',$order['employee_id']);
            $due->update(['due' => ($order['due']+$due->first()->due)]);
        }
    }
}
