<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Due;
class Collection extends Model
{
    protected $fillable = [
        'employee_id',
        'user_id',
        'paid'
    ];

    public function reductDue($request){
        $employee = Due::where('employee_id',$request->all()['employee_id'])->get()->first();
        $employee->update(['due'=>($employee->first()->due - $request->all()['paid'])]);
    }
}
