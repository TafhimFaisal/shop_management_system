<?php $title = __('title.user-history-collection')?>
@extends('layouts.master')
@section('content')
   <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table style="width:100%" id="user_order_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th>Employee Name</th>
                <th>Paid</th>
                <th>Entry Date</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th>Employee Name</th>
                <th>Paid</th>
                <th>Entry Date</th>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
   <script>
            $(function () {
            $("#user_order_table").DataTable({
                "ordering": true,
                "processing": true,
                "serverSide": true,
                "ajax":{
                        "url": '{{ url("datatable/users/history/collections/$id")}}',
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}" }
                        },
                "columns": [
                    { "data": "employee_name" },
                    { "data": "paid" },
                    { "data": "entry_date" },
                    
                ]
            });
        });
    </script> 
@endpush