<?php

namespace App\Http\Controllers;
use App\Http\Requests\EmployeeRequest;
use App\Employee;
use App\Order;
use App\Due;
use App\DueHistory;
use App\Collection;
use App\User;
use Illuminate\Http\Request;

use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Imports\EmployeesImport;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('employee.index')->with('employees',Employee::all());
    }

    public function datatable()
    {
        return datatables()
                ->of(Employee::all())
                ->addColumn('action', 
                            '<div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/data/employee/history/orders/{{$id}}">Order History</a>
                                    <a class="dropdown-item" href="/data/employee/history/dues/{{$id}}">Due History</a>
                                    <a class="dropdown-item" href="/data/employee/history/paid-dues/{{$id}}">Paid Due History</a>
                                    <a class="dropdown-item" href="/employee/{{$id}}/edit">Edit Information</a>
                                    </div>
                                </div>
                            </div>')->toJson();
    }

    

    public function create()
    {
        return view('employee.create');
    }

    public function store(EmployeeRequest $request)
    {
        Employee::create($request->all())->createDueHistory();
        return redirect('employee')->with('success','successfull');

    }

    public function edit(Employee $employee)
    {
        return view('employee.edit')->with('employee',$employee);
    }

    public function update(EmployeeRequest $request, Employee $employee)
    {
        $employee->update($request->all());
        return redirect('employee')->with('info','successfull');
    }

    public function datatableDue($id){
        return datatables()
                ->of(Order::where('employee_id',$id))
                ->addColumn('product_name','{{App\Product::findorFail($product_id)->name}}')
                ->toJson();
    }

    public function datatablePaidDue($id){
        return datatables()
                ->of(Collection::where('employee_id',$id))
                ->addColumn('user_name','{{App\User::findorFail($user_id)->name}}')
                ->toJson();
    }


    public function datatableOrder($id){
        return datatables()
                ->of(Order::where('employee_id',$id))
                ->addColumn('product','{{App\Product::findorFail($product_id)->name}}')
                ->addColumn('price','{{$unit*$per_unit_price}}')
                ->toJson();
    }

    public function order($id)
    {
        return view('employee.history.orders')
                ->with('id',$id)
                ->with('due',Due::where('employee_id',$id)->first()->due);
    }

    public function due($id)
    {
        return view('employee.history.due')
                ->with('id',$id)->with('due',Due::where('employee_id',$id)
                ->first()->due);
    }
    public function paiddue($id)
    {
        return view('employee.history.paiddue')
                ->with('id',$id)->with('due',Due::where('employee_id',$id)
                ->first()->due);
    }


    public function import(Request $request) 
    {
        Excel::import(new EmployeesImport, $request->employees);
        return redirect('/employee')->with('success', 'All good!');
    }

    public function option(Request $request)
    {
        $key = request()->searchTerm;
        if ($key!=null) {
            $results = Employee::query()->where('name','like',"$key%")->get();
            $data = [];
            foreach ($results as $key => $result) {
                $data[] = array(
                    "id"=>$result['id'],
                    "text"=>$result['name'],
                );
            }
            return json_encode($data);
        }
    }

    public function destroy(Employee $employee)
    {
        
    }
}
