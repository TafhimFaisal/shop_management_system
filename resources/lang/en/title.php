<?php

return [
    'home' => 'Dashboard',
    'product' => 'Product info',
    'stock' => 'Stocks info',

    'employee' => 'Employee list',
    'employee-create' => 'Create Employee',
    'employee-import' => 'Impost Employee',
    'employee-edit' => 'Edit Employee Info',
    'employee-history-due' =>'Due list',
    'employee-history-paiddue' =>'Paid Due list',
    'employee-history-order'=>'Order list',

    'user' => 'User list',
    'user-create' => 'Create User',
    'user-edit' => 'Edit User',
    'user-history-stock' =>'Stock list',
    'user-history-order'=>'Order list',
    'user-history-collection'=>'Collection list',
    
    'order' => 'Orders list',
    'collection' => 'Collections list',
    
];