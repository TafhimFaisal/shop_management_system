<?php $title = __('title.order')?>
@extends('layouts.master')
@section('content')
@include('layouts.includes.massage')
<div class="card card-default">
    <form action="\order" method="post">
        @csrf
        @method('post')
        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
        <div class="card-body">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="employee" class="col-form-label">Employee :</label>
                        <select id='employee' name="employee_id" class="form-control @error('employee_id') is-invalid @enderror">
                            <option value="">- Search Employee -</option>
                        </select>
                        @include('layouts.includes.errors',['name'=>'employee_id'])
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="product" class="col-form-label">Product :</label>
                        <select id='product' name="product_id" class="form-control @error('product_id') is-invalid @enderror">
                            <option value="">- Search Product -</option>
                        </select>
                        @include('layouts.includes.errors',['name'=>'product_id'])
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="quantity" class="col-form-label">Quantity :</label>
                        <input name="unit" value = '{{ old('unit') }}' type="number" class="form-control @error('unit') is-invalid @enderror" id="quantity" placeholder="Enter quantity..">
                        @include('layouts.includes.errors',['name'=>'unit'])
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="price" class="col-form-label">Per-Unit Price:</label>
                        <input name="per_unit_price" value = '{{ old('per_unit_price') }}' type="number" class="form-control @error('per_unit_price') is-invalid @enderror" id="price" placeholder="Enter price per unit..">
                        @include('layouts.includes.errors',['name'=>'per_unit_price'])
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="totelAmount" class="col-form-label">Totel Amount :</label>
                        <input type="number" disabled class="form-control" id="totelAmount">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="price" class="col-form-label">Payment :</label>
                        <input name="paid" value ='{{ old('paid') }}'  type="number" class="form-control @error('paid') is-invalid @enderror" id="payment" placeholder="Enter price per unit..">
                        @include('layouts.includes.errors',['name'=>'paid'])
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="due" class="col-form-label">Due :</label>
                        <input name="due" type="number" value="" disabled class="form-control" id="due">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Add Order</button>
        </div>
    </form>
</div>
@endsection
@push('scripts')
    <script>
        $('#price').change(
            function(){
                var perUnitPrice = $(this).val();
                var unit = $('#quantity').val();
                $('#totelAmount').val(perUnitPrice*unit); 
            }
        )
        $('#quantity').change(
            function(){
                var unit = $(this).val();
                var perUnitPrice = $('#price').val();
                $('#totelAmount').val(perUnitPrice*unit); 
            }
        )

        $('#payment').change(
            function(){
                var payment = $(this).val();
                var totelAmount = $('#totelAmount').val();
                $('#due').val(totelAmount-payment); 
            }
        )

        $('#quantity').change(
            function(){
                var quantity = $(this).val();
                var totelAmount = $('#totelAmount').val();
                $('#due').val(totelAmount-payment); 
            }
        )

        var payment = $("#price").val();
        var totelAmount = $('#totelAmount').val();
        $('#due').val(totelAmount-payment); 

        $(document).ready(function(){

            $("#employee").select2({
                ajax: { 
                    url: '{{ url("search/employee/option")}}',
                    // type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                    },
                    processResults: function (response) {
                    return {
                        results: response
                    };
                    },
                    cache: false
                },
                theme: 'bootstrap4'
            });
            $("#product").select2({
                ajax: { 
                    url: '{{ url("search/products/option")}}',
                    // type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                    },
                    processResults: function (response) {
                    return {
                        results: response
                    };
                    },
                    cache: false
                },
                theme: 'bootstrap4'
            });

        });

    </script>
@endpush