<?php $title = __('title.user-edit')?>
@extends('layouts.master')
@section('content')
<div class="card card-default">
    <form action="\user\{{$user->id}}" method="post">
        @csrf
        @method('put')
        <input type="hidden" name="id" value="{{$user->id}}">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Name:</label>
                        <div class="">
                            <input type="text" value="{{ old('name') ? old('name') : $user->name }}" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter name..">
                            @include('layouts.includes.errors',['name'=>'name'])
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email:</label>
                        <div class="">
                            <input type="text" value="{{old('email') ? old('email') : $user->email }}" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter Email..">
                            @include('layouts.includes.errors',['name'=>'email'])
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone" class="col-form-label">Phone :</label>
                        <div class="">
                            <input type="text" value="{{ old('phone') ? old('phone') : $user->phone }}" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="Enter Phone Number..">
                            @include('layouts.includes.errors',['name'=>'phone'])
                        </div>
                    </div>
                </div>
                {{-- <div class="col-12">
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password :</label>
                        <div class="">
                            <input type="password" value="{{ old('password')  }}" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Enter password...">
                            @include('layouts.includes.errors',['name'=>'password'])
                        </div>
                    </div>
                </div> --}}
                
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Update</button>
            <a href="#" role="button"  class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Change Password</a>
        </div>
    </form>
</div>
   
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/change/user/password" method="post">
            <div class="modal-body">
                @csrf
                @method('post')
                <input type="hidden" name="id" value="{{$user->id}}">
                <label for="newPassword" class="col-form-label">Password :</label>
                <input type="password" name="password" class="form-control" id="new_inputPassword" placeholder="Password">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection