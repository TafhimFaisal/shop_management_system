<?php $title = __('title.home')?>
@extends('layouts.master')
@section('content')
    <div class="container-fluid pb-5">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$totale_price}}</h3>
                    <p>Sells</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="/home" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$totale_due}}</h3>
                    <p>Due</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/due" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$totale_user}}</h3>
                    <p>User </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="/user" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-secondary">
                <div class="inner">
                    <h3>{{$totale_employee}}</h3>
                    <p>Employee</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="/employee" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- Small boxes (end box) -->
        
        <!-- Small boxes (end box) -->
        <div class="card">
            <div class="card-body">
                <table style="width:100%" id="due_sells_table" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Due</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                        <th>Employee</th>
                        <th>Due</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- Small boxes (end box) -->

    </div>
   
@endsection
@push('scripts')
    <script>
       $(function () {
            $("#due_sells_table").DataTable({
                "ordering": true,
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url":"{{url('due/datatable')}}",
                    "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "employee"},
                    { "data": "due"}
                ]
            });
        });
        
    </script>
@endpush