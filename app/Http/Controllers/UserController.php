<?php

namespace App\Http\Controllers;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user.index');
    }

    public function datatable()
    {
        return datatables()->of(User::all())->addColumn(
            'action', 
            '<div class="btn-group">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/data/users/history/stocks/{{$id}}">Stock History</a>
                        <a class="dropdown-item" href="/data/users/history/orders/{{$id}}">Order History</a>
                        <a class="dropdown-item" href="/data/users/history/collections/{{$id}}">Collection History</a>
                        <a class="dropdown-item" href="/user/{{$id}}/edit">Edit Information</a>
                    </div>
                </div>
            </div>')->toJson();
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=> Hash::make($request->password)
        ]);
        return redirect('user')->with('success','successfull');
    }

    public function edit(User $user)
    {
        return view('user.edit')->with('user',$user);
    }

    public function update(UserRequest $request, User $user)
    {
        $user->update($request->all());
        return redirect('user')->with('info','successfull');
    }

    //---history view---//
    public function stock($id)
    {
        return view("user.history.stocks")->with('id',$id);
    }
    public function order($id)
    {
        return view("user.history.orders")->with('id',$id);
    }
    public function collection($id)
    {
        return view("user.history.collections")->with('id',$id);
    }
    //---history view---//

    //---history---//
    public function datatableStock($id)
    {
        return datatables()->of(User::findorFail($id)->stockHistory)
                                ->addColumn('product_name','{{App\Product::findorFail($product_id)->name}}')
                                ->addColumn('quantity','{{$quantity}}')
                                ->addColumn('entry_date','{{$created_at}}')
                                ->toJson();
    }
    public function datatableOrder($id)
    {
        return datatables()->of( User::findorFail($id)->order)
                                ->addColumn('product_name','{{App\Product::findorFail($product_id)->name}}')
                                ->addColumn('employee_name','{{App\Employee::findorFail($employee_id)->name}}')
                                ->addColumn('price','{{$per_unit_price*$unit}}')
                                ->addColumn('paid','{{$paid}}')
                                ->addColumn('due','{{$due}}')
                                ->addColumn('entry_date','{{$created_at}}')
                                ->toJson();
    }
    public function datatableCollection($id)
    {
        return datatables()->of( User::findorFail($id)->collection)
                            ->addColumn('employee_name','{{App\Employee::findorFail($employee_id)->name}}')
                            ->addColumn('paid','{{$paid}}')
                            ->addColumn('entry_date','{{$created_at}}')
                            ->toJson();
    }
    //---history---//

    public function changePassword(Request $request){
        $user = User::findorFail($request->id);
        $user->update([
            'password' => Hash::make($request->password)
        ]);
        return redirect("\user")->with('success','Password has successfully been updated');
    }
    public function destroy(User $user)
    {
        
    }
}
