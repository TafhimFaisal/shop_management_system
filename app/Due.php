<?php

namespace App;
use App\Employee;
use Illuminate\Database\Eloquent\Model;

class Due extends Model
{
    protected $fillable = [
        'due',
        'employee_id'
    ];

    public function employee(){
        $this->belongsTo(Employee::class);
    }
}
