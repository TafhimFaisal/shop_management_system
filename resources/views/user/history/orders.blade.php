<?php $title = __('title.user-history-order')?>
@extends('layouts.master')
@section('content')
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table style="width:100%" id="user_order_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Employee Name</th>
                <th>Price</th>
                <th>Paid</th>
                <th>Due</th>
                <th>Entry Date</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th>Product Name</th>
                <th>Employee Name</th>
                <th>Price</th>
                <th>Paid</th>
                <th>Due</th>
                <th>Entry Date</th>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
    
@endsection
@push('scripts')
    <script>
            $(function () {
            $("#user_order_table").DataTable({
                "ordering": true,
                "processing": true,
                "serverSide": true,
                "ajax":{
                        "url": '{{ url("datatable/users/history/orders/$id")}}',
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}" }
                        },
                "columns": [
                    { "data": "product_name" },
                    { "data": "employee_name" },
                    { "data": "price" },
                    { "data": "paid" },
                    { "data": "due" },
                    { "data": "entry_date" }
                ]
            });
        });
    </script>
@endpush