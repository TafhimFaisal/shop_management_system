<?php

namespace App\Imports;

use App\Employee;
use Maatwebsite\Excel\Concerns\ToModel;
use DB;

class EmployeesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {  
        if(!Employee::where('phone','=',$row[2])->first()){
            $employee = new Employee;
            return $employee->create([
                'name'  =>   $row[0],
                'email' =>   $row[1],
                'phone' =>   $row[2]
            ])->createDueHistory();
        }
    }
}
