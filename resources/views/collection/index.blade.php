<?php $title = __('title.collection')?>
@extends('layouts.master')
@section('content')
@include('layouts.includes.massage')
<div class="card card-default">
    <form action="/collection" method="post">
        @csrf
        @method('post')
        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="employee" class="col-form-label">Employee :</label>
                        <select id='employee' name="employee_id" class="form-control @error('employee_id') is-invalid @enderror">
                            <option value="">- Search Employee -</option>
                        </select>
                        @include('layouts.includes.errors',['name'=>'employee_id'])
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="due" class="col-form-label">Due :</label>
                        <div class="input-group input-group-md">
                            <input disabled id="due" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label for="pament" class="col-form-label">Pament :</label>
                        <div class="">
                            <input name="paid" type="number" class="form-control @error('paid') is-invalid @enderror" id="pament" placeholder="Enter payment..">
                            @include('layouts.includes.errors',['name'=>'paid'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Collect Money</button>
        </div>
    </form>
</div>

@endsection
@push('scripts')

<script>
    $(document).ready(function(){

            $("#employee").select2({
                ajax: { 
                    url: '{{ url("search/employee/option")}}',
                    // type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                    },
                    processResults: function (response) {
                    return {
                        results: response
                    };
                    },
                    cache: false
                },
                theme: 'bootstrap4'
            });
            

            $('#employee').change(
                function(){
                    var employee_id = $("#employee").val();
                    $.ajax({
                        url:'{{ url("search/due")}}',
                        data:{query:employee_id},
                        success:function(result){
                            $("#due").val(result);
                        }
                    })
                }
            )
        });
</script>  
@endpush