<?php $title = __('title.employee')?>
@extends('layouts.master')
@section('content')
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table style="width:100%" id="employee_due_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th>User Name</th>
                <th>Quantity</th>
                <th>Entry Date</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th>User Name</th>
                <th>Quantity</th>
                <th>Entry Date</th>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
       $(function () {
        $("#employee_due_table").DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": '{{ url("datatable/product/history/sells/$id")}}',
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}" }
                   },
            "columns": [
                { "data": "user_name" },
                { "data": "unit" },
                { "data": "created_at" }
              ]
        });
      });
    </script>
@endpush