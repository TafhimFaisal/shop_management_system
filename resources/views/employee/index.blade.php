<?php $title = __('title.employee')?>
@extends('layouts.master')
@section('content')
@include('layouts.includes.massage')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9 col-sm-6">
                    <h3 class="card-title"></h3>
                </div>
                <div class="col-md-1 col-sm-2">
                    <a role="button" href="/employee/create" class="btn btn-success btn-sm text-light" style="width:100%">Add</a>
                </div>
                <div class="col-md-2 col-sm-4">
                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" style="width:100%" data-target="#exampleModal">Add in bulk</button>
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table style="width:100%" id="employee_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
              <th>ID</th>
              <th width="50%">Name</th>
              <th>Phone</th>
              <th>email</th>
              <th width="15%">Action</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>email</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/import/employee" method="post" enctype="multipart/form-data">
          @csrf
          @method("post")
          <div class="custom-file">
            <input type="file" name="employees" class="custom-file-input" id="validatedCustomFile" required>
            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
            <div class="invalid-feedback">Example invalid custom file feedback</div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Import</button>
      </div>
      </form>
    </div>
  </div>
</div>


@endsection
@push('scripts')
    <script>
       $(function () {
        $("#employee_table").DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('data/employees') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "phone" },
                { "data": "email" },
                { "data": "action" }
              ]
        });
    });
    </script>
@endpush



