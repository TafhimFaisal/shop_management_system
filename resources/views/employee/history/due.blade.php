<?php $title = __('title.employee-history-due')?>
@extends('layouts.master')
@section('content')
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <h1> Remaining Due : {{$due}}</h1>
            <table style="width:100%" id="employee_due_table" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Due</th>
                <th>Entry Date</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th>Product Name</th>
                <th>Due</th>
                <th>Entry Date</th>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
       $(function () {
        $("#employee_due_table").DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": '{{ url("datatable/employee/history/dues/$id")}}',
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}" }
                   },
            "columns": [
                { "data": "product_name" },
                { "data": "due" },
                { "data": "created_at" }
              ]
        });
      });
    </script>
@endpush