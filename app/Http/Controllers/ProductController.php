<?php

namespace App\Http\Controllers;

use App\Product;
use App\Order;
use App\User;
use App\Stock;
use App\StockHistory;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        
        return view('product.index');
    }

    public function datatable()
    {
        return datatables()->of(Product::all())
                        ->addColumn('quantity','{{App\Product::findorFail($id)->stock->quantity}}')
                        ->addColumn('action', 
                        '<div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/data/product/history/stocks/{{$id}}">stock History</a>
                                    <a class="dropdown-item" href="/data/product/history/sells/{{$id}}">Sells History</a>
                                    <a class="dropdown-item" href="/product/{{$id}}/edit">Edit Information</a>
                                </div>
                            </div>
                        </div>')
                        ->toJson();
    }

    

    public function store(ProductRequest $request)
    {
        Product::create($request->all())->addStock();
        return redirect('product')->with('success','successfull');
    }

    public function edit(Product $product)
    {
        return view('product.index')->with('product',$product);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product->update($request->all());
        return redirect('product')->with('success','successfull');
    }

    public function sells($id)
    {
        return view('product.history.sells')->with('id',$id);
    }
    public function stocks($id)
    {
        
        return view('product.history.stocks')->with('id',$id);
        
    }
    public function datatableStocks($id)
    {
        return datatables()->of(StockHistory::where('product_id',$id))
                        ->addColumn('user_name','{{App\User::findorFail($user_id)->name}}')
                        ->toJson();
    }
    public function datatableSells($id)
    {
        return datatables()->of(Order::where('product_id',$id))
                        ->addColumn('user_name','{{App\User::findorFail($user_id)->name}}')
                        ->toJson();
    }

    

    public function option(Request $request)
    {
        $key = request()->searchTerm;
        if ($key!=null) {
            $results = Product::query()->where('name','like',"$key%")->get();
            $data = [];
            foreach ($results as $key => $result) {
                $data[] = array(
                    "id"=>$result['id'],
                    "text"=>$result['name'],
                );
            }
            return json_encode($data);
        }
    }

    public function destroy(Product $product)
    {
        
    }
}
